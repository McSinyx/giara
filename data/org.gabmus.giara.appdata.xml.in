<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
    <id>org.gabmus.giara</id>
    <name>Giara</name>
    <developer_name>Gabriele Musco</developer_name>
    <summary>A GTK app for Reddit</summary>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0+</project_license>
    <!--recommends>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </recommends-->
    <description>
        <p>Giara is a GTK app for Reddit</p>
    </description>
    <launchable type="desktop-id">org.gabmus.giara.desktop</launchable>
    <screenshots>
        <screenshot type="default">
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/mainwindow.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot01.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot02.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot03.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot04.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot05.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot06.png</image>
            <image>https://gitlab.gnome.org/gabmus/giara/raw/website/website/screenshots/scrot07.png</image>
        </screenshot>
    </screenshots>
    <url type="homepage">https://giara.gabmus.org</url>
    <url type="bugtracker">https://gitlab.gnome.org/gabmus/giara/issues</url>
    <update_contact>gabmus@disroot.org</update_contact>
    <releases>
        <release version="0.2" timestamp="1603273834">
            <description>
                <ul>
                    <li>Improved markdown rendering</li>
                    <li>Dark mode implemented</li>
                    <li>Added option to disable images in post previews</li>
                    <li>Added option to set a maximum size for images</li>
                    <li>Added option to clear cache</li>
                    <li>Added Brazilian Portuguese translation</li>
                    <li>New post or comment window now remembers its size</li>
                    <li>Subreddits can now be searched when creating new posts</li>
                    <li>Initial support for multireddits</li>
                    <li>Added Croatian translation</li>
                    <li>Added notifications for unread inbox items</li>
                    <li>Made comment border lines colorful</li>
                    <li>Authentication is now handled by your default browser</li>
                </ul>
            </description>
        </release>
        <release version="0.1.1" timestamp="1602403522">
            <description>
                <ul>
                    <li>Improvements for flatpak packaging</li>
                </ul>
            </description>
        </release>
        <release version="0.1" timestamp="1589880777">
            <description>
                <ul>
                    <li>First release</li>
                </ul>
            </description>
        </release>
    </releases>
    <content_rating type="oars-1.1">
        <content_attribute id="social-chat">moderate</content_attribute>
    </content_rating>

</component>
