#!/bin/bash

pwd
rm -rf build
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/build/testdir
ninja
ninja install