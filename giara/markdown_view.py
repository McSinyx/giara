from gettext import gettext as _
from gi.repository import Gtk, Pango
from bs4 import BeautifulSoup
import mistune

HEADER_LEVELS = [
    'xx-large', 'x-large', 'large',
    'medium', 'small', 'x-small', 'xx-small'
]


class PangoRenderer(mistune.Renderer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, escape=True, **kwargs)

    def text(self, text):
        return mistune.escape(text)

    def link(self, link, title, text):
        return super().link(link, None, text)

    def image(self, src, alt='', title=None):
        return self.link(
            src,
            None,
            _('Image: ')+alt if isinstance(alt, str) else _('[Image]')
        )

    def emphasis(self, text):
        return f'<i>{text}</i>'

    def double_emphasis(self, text):
        return f'<b>{text}</b>'

    def codespan(self, text):
        return (
            '<tt>{0}</tt>'
        ).format(mistune.escape(text.rstrip(), smart_amp=False))

    def block_code(self, code, lang=None):
        return f'\n<tt>{mistune.escape(code)}</tt>\n'

    def newline(self):
        return '\n'

    def list(self, body, ordered=True):
        return '\n'.join([
            li.replace('\u2022', f'{i+1}.') for i, li in
            enumerate(body.split('\n'))
        ]) if ordered else body

    def list_item(self, text):
        # u2022 is a bullet sign
        return f'\u2022 {text}\n'

    def paragraph(self, text):
        return f'{text}\n'

    def strikethrough(self, text):
        return f'<span strikethrough="true">{text}</span>'

    def linebreak(self):
        return '\n'

    def hrule(self):
        return '\n@@##HR##@@\n'

    def block_quote(self, text):
        return mistune.escape(text)
        # return f'\n@STARTBLOCKQUOTE@\n{text}\n@ENDBLOCKQUOTE@'

    def table(self, header, body):
        return (
            f'\n{header}\n{body}\n'
        )

    def table_row(self, content):
        return f'{content}\n'

    def table_cell(self, content, **flags):
        return content + '\t\t\t'

    def header(self, text, level, raw=None):
        if level > len(HEADER_LEVELS):
            level = 0
        return '\n<span size="{0}">{1}</span>\n'.format(
            HEADER_LEVELS[level-1],
            text
        )


class MarkdownView(Gtk.Box):
    def __init__(self, markdown='', **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.markdown = ''
        self.pango_strings = []
        self.parser = mistune.Markdown(renderer=PangoRenderer())
        self.set_text(markdown)

    def empty(self):
        for child in self.get_children():
            self.remove(child)

    def append_label(self, pstr):
        pstr = str(BeautifulSoup(
            pstr,
            'html.parser'
        ))
        label = Gtk.Label()
        label.set_use_markup(True)
        label.set_markup(pstr)
        label.set_selectable(True)
        label.set_line_wrap(True)
        label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
        label.set_justify(Gtk.Justification.FILL)
        self.add(label)
        label.set_vexpand(False)
        label.set_hexpand(True)
        label.set_halign(Gtk.Align.START)

    def append_hr(self):
        separator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        separator.get_style_context().add_class('separator-hr')
        self.add(separator)
        separator.set_vexpand(False)
        separator.set_hexpand(True)

    def set_text(self, markdown):
        self.empty()
        self.markdown = markdown
        if markdown == '':
            return
        self.pango_strings = self.parser(self.markdown).split('@@')
        for pstr in self.pango_strings:
            if pstr == '##HR##':
                self.append_hr()
            else:
                self.append_label(pstr.strip())
        self.show_all()
