from gi.repository import Gtk, Handy, GObject


class SqueezingViewSwitcherHeaderbar(Handy.WindowHandle):
    __gsignals__ = {
       'squeeze': (
           GObject.SignalFlags.RUN_FIRST,
           None,
           (bool,)
        )
    }

    def __init__(self, builder, stack, **kwargs):
        super().__init__(**kwargs)
        self.builder = builder
        self.stack = stack
        self.headerbar = self.builder.get_object('headerbar')
        self.squeezer = self.builder.get_object('squeezer')

        self.view_switcher = Handy.ViewSwitcher()
        self.view_switcher.set_policy(Handy.ViewSwitcherPolicy.WIDE)
        self.squeezer.add(self.view_switcher)
        self.squeezer.add(Gtk.Label())
        self.squeezer.connect(
            'notify::visible-child',
            lambda *args: self.emit(
                'squeeze',
                self.squeezer.get_visible_child() != self.view_switcher
            )
        )
        self.view_switcher.set_valign(Gtk.Align.FILL)
        self.view_switcher.set_stack(self.stack)

        self.add(self.headerbar)
