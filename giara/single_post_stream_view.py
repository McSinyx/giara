from gi.repository import Gtk
from giara.sections_stack import SectionsStack
from giara.single_post_stream_headerbar import SinglePostStreamHeaderbar


class SinglePostStreamView(Gtk.Box):
    def __init__(self, generator, name, title, show_post_func, load_now=True,
                 **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.generator = generator
        self.name = name
        self.title = title

        # it's a stack, but I'm just gonna use one child of it.
        # I mostly care about the whole structure for this particular case
        self.section_stack = SectionsStack(
            [{
                'name': self.name,
                'title': self.title,
                'gen': self.generator
            }],
            show_post_func,
            load_now=load_now
        )
        self.headerbar = SinglePostStreamHeaderbar(self.title)
        self.add(self.headerbar)
        self.headerbar.set_vexpand(False)
        self.headerbar.set_hexpand(True)
        self.add(self.section_stack)
        self.section_stack.set_vexpand(True)

        self.headerbar.refresh_btn.connect(
            'clicked',
            self.refresh
        )

    def refresh(self, *args):
        # self.headerbar.refresh_btn.set_sensitive(False)
        self.section_stack.refresh()
        # self.headerbar.refresh_btn.set_sensitive(True)
