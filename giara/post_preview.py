from giara.constants import RESOURCE_PREFIX
from giara.common_post_box import CommonPostBox
from giara.confManager import ConfManager
from gi.repository import Gtk, GLib
from threading import Thread
from praw.models import Submission


class PostPreview(CommonPostBox):
    def __init__(self, post, **kwargs):
        self.confman = ConfManager()
        super().__init__(
            post,
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/post_preview.glade'
            ),
            **kwargs
        )
        self.comments_box = self.builder.get_object('comments_box')
        self.comments_label = self.builder.get_object('comments_label')
        GLib.idle_add(self.count_comments)

        def hide_show_image(*args):
            img_cont = self.builder.get_object('image_container')
            if self.confman.conf['show_thumbnails_in_preview']:
                img_cont.set_visible(True)
                img_cont.set_no_show_all(False)
                if len(img_cont.get_children()) <= 0:
                    self.set_post_image()
            else:
                img_cont.set_visible(False)
                img_cont.set_no_show_all(True)

        self.confman.connect(
            'on_show_thumbnails_in_preview_changed',
            hide_show_image
        )

    def set_post_image(self):
        if self.confman.conf['show_thumbnails_in_preview']:
            super().set_post_image()

    def count_comments(self):

        def af():
            if isinstance(self.post, Submission):
                num_comments = str(len(self.post.comments))
                GLib.idle_add(cb, num_comments)

        def cb(num_comments):
            self.comments_box.set_visible(True)
            self.comments_box.set_no_show_all(False)
            self.comments_label.set_text(num_comments)
            self.comments_box.show_all()

        Thread(target=af).start()


class PostPreviewListboxRow(Gtk.ListBoxRow):
    def __init__(self, post, **kwargs):
        super().__init__(**kwargs)
        self.post = post

        def lazy():
            self.post_preview = PostPreview(post)
            self.add(self.post_preview)
            self.show_all()

        GLib.idle_add(lazy, priority=GLib.PRIORITY_LOW)


class PostPreviewListbox(Gtk.ListBox):
    def __init__(self, post_gen_func, show_post_func, load_now=True, **kwargs):
        super().__init__(**kwargs)
        self.get_style_context().add_class('card')
        self.set_selection_mode(Gtk.SelectionMode.NONE)
        self.post_gen_func = post_gen_func
        self.show_post_func = show_post_func
        self.post_gen = None
        self.initialized = False
        self.loading = False
        if load_now:
            self.refresh()
        self.connect('row-activated', self.on_row_activate)

        self.set_header_func(self.separator_header_func)
        self.stop_loading = False

    def set_gen_func(self, n_gen_func):
        self.post_gen_func = n_gen_func

    def separator_header_func(self, row, prev_row):
        if (
            prev_row is not None and
            row.get_header() is None
        ):
            row.set_header(
                Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
            )

    def empty(self, *args):
        while True:
            row = self.get_row_at_index(0)
            if row:
                self.remove(row)
            else:
                break

    def refresh(self, *args):
        self.initialized = True
        self.empty()
        self.post_gen = self.post_gen_func(limit=None)
        self.load_more()

    def _on_post_preview_row_loaded(self, target):
        row = PostPreviewListboxRow(target)
        self.add(row)

    def _async_create_post_preview_row(self, gen, num):
        for n in range(num):
            if self.stop_loading:
                break
            try:
                target = next(gen)
                GLib.idle_add(
                    self._on_post_preview_row_loaded, target,
                    priority=GLib.PRIORITY_LOW
                )
            except StopIteration:
                # TODO: add child indicating post stream end
                break
        self.loading = False

    def load_more(self, num=10):
        if not self.stop_loading and self.loading:
            return
        self.loading = True
        t = Thread(
            target=self._async_create_post_preview_row,
            args=(self.post_gen, num)
        )
        self.stop_loading = False
        t.start()

    def on_row_activate(self, lb, row):
        if hasattr(row, 'post') and row.post is not None:
            self.show_post_func(row.post)
