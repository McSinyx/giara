from gettext import gettext as _
from gi.repository import Gtk, Handy
from giara.sections_stack import SectionsStack
from giara.front_page_headerbar import FrontPageHeaderbar
from giara.single_post_stream_view import SinglePostStreamView
from giara.subreddits_list_view import SubredditsListView
from giara.subreddit_view import SubredditView
from giara.search_view import SearchView
from giara.user_view import UserView
from giara.confManager import ConfManager
from giara.subreddit_search_view import SubredditSearchView
from giara.accel_manager import add_mouse_button_accel
from praw.models import Redditor
from giara.multireddit_list_view import MultiredditsListView
from giara.multireddit_view import MultiredditView
from giara.inbox_view import InboxListView


class LeftStack(Gtk.Stack):
    def __init__(self, show_post_func, **kwargs):
        super().__init__(**kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit
        self.set_transition_type(Gtk.StackTransitionType.CROSSFADE)
        self.show_post_func = show_post_func

        # Child 1: Front page stack and respective headerbar
        self.front_page_view = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.front_page_stack = SectionsStack([
            {
                'name': 'best',
                'title': _('Best'),
                'gen': self.reddit.front.best,
                'icon': 'best-symbolic'
            },
            {
                'name': 'hot',
                'title': _('Hot'),
                'gen': self.reddit.front.hot,
                'icon': 'hot-symbolic'
            },
            {
                'name': 'new',
                'title': _('New'),
                'gen': self.reddit.front.new,
                'icon': 'new-symbolic'
            }
        ], show_post_func, load_now=False)
        self.front_page_stack.set_visible_child_name(
            self.confman.conf['default_front_page_view']
        )
        self.front_page_stack.on_visible_child_change()
        self.front_page_bottom_bar = Handy.ViewSwitcherBar()
        self.front_page_bottom_bar.set_stack(self.front_page_stack)
        self.front_page_headerbar = FrontPageHeaderbar(
            self.front_page_stack, self.reddit
        )
        self.front_page_view.headerbar = self.front_page_headerbar
        self.front_page_view.add(self.front_page_headerbar)
        self.front_page_headerbar.set_vexpand(False)
        self.front_page_headerbar.set_hexpand(True)
        self.front_page_headerbar.refresh_btn.connect(
            'clicked',
            self.front_page_stack.refresh
        )
        self.front_page_view.add(self.front_page_stack)
        self.front_page_stack.set_hexpand(True)
        self.front_page_stack.set_vexpand(True)
        self.front_page_view.add(self.front_page_bottom_bar)
        self.front_page_bottom_bar.set_vexpand(False)
        self.front_page_headerbar.connect(
            'squeeze',
            lambda caller, squeezed: self.front_page_bottom_bar.set_reveal(
                squeezed
            )
        )
        self.add_titled(
            self.front_page_view,
            'front_page',
            _('Front page')
        )

        # Child 2: Saved items stack (forcedly a stack to preserve structure)
        self.saved_view = SinglePostStreamView(
            self.reddit.user.me().saved,
            'saved',
            _('Saved posts'),
            show_post_func
        )
        self.add_titled(
            self.saved_view,
            'saved_view',
            _('Saved posts')
        )

        self.front_page_view.headerbar.go_saved_btn.connect(
            'clicked',
            lambda *args: self.set_visible_child(self.saved_view)
        )

        # Child 3: Profile stack (forcedly a stack to preserve structure)
        self.profile_view = SinglePostStreamView(
            self.reddit.user.me().new,
            'profile',
            _('Profile'),
            show_post_func,
            load_now=False
        )
        self.add_titled(
            self.profile_view,
            'profile_view',
            _('Profile')
        )

        def on_go_profile(*args):
            self.set_visible_child(self.profile_view)
            self.profile_view.section_stack.on_visible_child_change()

        self.front_page_view.headerbar.go_profile_btn.connect(
            'clicked',
            on_go_profile
        )

        # Child 4: Subreddits list view
        self.subreddits_list_view = SubredditsListView(
            lambda *args, **kwargs: self.reddit.user.subreddits(
                *args, **kwargs
            ),
            self.show_subreddit_func, sort=True
        )
        self.add_titled(
            self.subreddits_list_view,
            'subreddits',
            _('Subreddits')
        )

        self.front_page_view.headerbar.go_subreddits_btn.connect(
            'clicked',
            lambda *args: self.set_visible_child(self.subreddits_list_view)
        )

        # Child 5: Single subreddit view, initialized in show_subreddit_func
        self.single_subreddit_view = None
        # Child 6: Single subreddit search view
        # closely related to 5, created along 5
        self.single_subreddit_search_view = None

        # Child 7: Search view
        self.search_view = SearchView(
            show_post_func,
            lambda *args, **kwargs: self.show_subreddit_func(
                *args, **kwargs, prev_view=self.search_view
            )
        )
        self.add_titled(
            self.search_view,
            'search',
            _('Search')
        )
        self.front_page_view.headerbar.search_btn.connect(
            'clicked',
            lambda *args: self.set_visible_child(self.search_view)
        )

        # Child 8: Inbox view
        self.inbox_view = InboxListView(
            self.reddit.inbox.all,
            show_post_func,
            load_now=False
        )
        self.add_titled(
            self.inbox_view,
            'inbox_view',
            _('Inbox')
        )

        self.front_page_view.headerbar.go_inbox_btn.connect(
            'clicked',
            self.on_go_inbox
        )

        # Child 9: Multireddits list view
        self.multireddits_list_view = MultiredditsListView(
            self.show_multireddit_func
        )
        self.add_titled(
            self.multireddits_list_view,
            'multireddits',
            _('Multireddits')
        )

        self.front_page_view.headerbar.go_multireddits_btn.connect(
            'clicked',
            lambda *args: self.set_visible_child(self.multireddits_list_view)
        )

        # Child 10: Single multireddit view,
        # initialized in show_multireddit_func
        self.single_multireddit_view = None

        for view in (
                self.saved_view, self.profile_view, self.subreddits_list_view,
                self.search_view, self.inbox_view, self.multireddits_list_view
        ):

            def go_back(*args):
                self.set_visible_child(self.front_page_view)

            def on_mouse_event(gesture, n_press, x, y):
                if gesture.get_current_button() == 8:  # Mouse back btn
                    go_back()

            view.headerbar.back_btn.connect('clicked', go_back)
            # the mouse accelerator needs to be kept around, thus this useless
            # assignment
            view.mouse_btn_accel = add_mouse_button_accel(
                view, on_mouse_event
            )

    def on_go_inbox(self, *args):
        self.set_visible_child(self.inbox_view)
        self.inbox_view.listbox.refresh()

    def get_headerbar(self):
        return self.get_visible_child().headerbar

    def show_multireddit_func(self, multi):
        if self.single_multireddit_view is not None:
            self.remove(self.single_multireddit_view)
        self.single_multireddit_view = MultiredditView(
            multi, self.show_post_func
        )

        self.add_titled(
            self.single_multireddit_view,
            multi.display_name,
            multi.display_name
        )

        def back_to_prev(*args):
            self.set_visible_child(self.multireddits_list_view)

        def on_mouse_event(gesture, n_press, x, y):
            if gesture.get_current_button() == 8:  # Mouse back btn
                back_to_prev()

        self.single_multireddit_view.headerbar.back_btn.connect(
            'clicked', back_to_prev
        )
        self.single_multireddit_view.mouse_btn_accel = add_mouse_button_accel(
            self.single_multireddit_view, on_mouse_event
        )

        self.single_multireddit_view.show_all()
        self.set_visible_child(self.single_multireddit_view)

    def show_subreddit_func(self, sub, prev_view=None):
        if prev_view is None:
            prev_view = self.subreddits_list_view
        for view in (
                self.single_subreddit_view,
                self.single_subreddit_search_view
        ):
            if view is not None:
                self.remove(view)
        if isinstance(sub, Redditor) or '/user/' in sub.url:
            redditor = sub
            if not isinstance(sub, Redditor):
                redditor = self.reddit.redditor(
                    sub.display_name_prefixed.replace('u/', '')
                )
            self.single_subreddit_view = UserView(
                redditor,
                self.show_post_func
            )
        else:
            self.single_subreddit_view = SubredditView(
                sub, self.show_post_func
            )
            self.single_subreddit_search_view = SubredditSearchView(
                sub, self.show_post_func
            )

            def back_to_sub(*args):
                self.set_visible_child(self.single_subreddit_view)

            def on_mouse_event(gesture, n_press, x, y):
                if gesture.get_current_button() == 8:  # Mouse back btn
                    back_to_sub()

            self.single_subreddit_search_view.headerbar.back_btn.connect(
                'clicked', back_to_sub
            )
            self.single_subreddit_search_view.mouse_btn_accel = \
                add_mouse_button_accel(
                    self.single_subreddit_search_view, on_mouse_event
                )

            def show_search():
                self.single_subreddit_search_view.show_all()
                self.set_visible_child(
                    self.single_subreddit_search_view
                )

            self.single_subreddit_view.headerbar.search_btn.connect(
                'clicked', lambda *args: show_search()
            )
            self.add_titled(
                self.single_subreddit_search_view,
                f'search_{sub.display_name}',
                _('Searching in {0}').format(sub.display_name_prefixed)
            )

        self.add_titled(
            self.single_subreddit_view,
            sub.display_name,
            sub.display_name_prefixed
        )

        def back_to_prev(*args):
            self.set_visible_child(prev_view)

        def on_mouse_event(gesture, n_press, x, y):
            if gesture.get_current_button() == 8:  # Mouse back btn
                back_to_prev()

        self.single_subreddit_view.headerbar.back_btn.connect(
            'clicked', back_to_prev
        )
        self.single_subreddit_view.mouse_btn_accel = add_mouse_button_accel(
            self.single_subreddit_view, on_mouse_event
        )

        self.single_subreddit_view.show_all()
        self.set_visible_child(self.single_subreddit_view)
