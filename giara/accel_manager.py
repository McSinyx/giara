from gi.repository import Gtk


def add_accelerators(window, shortcuts_l: list):
    accel_group = Gtk.AccelGroup()
    window.add_accel_group(accel_group)
    for s in shortcuts_l:
        __add_accelerator(accel_group, s['combo'], s['cb'])


def __add_accelerator(accel_group, shortcut, callback):
    if shortcut:
        key, mod = Gtk.accelerator_parse(shortcut)
        accel_group.connect(
            key, mod, Gtk.AccelFlags.VISIBLE, callback
        )


def add_mouse_button_accel(widget, function):
    '''Adds an accelerator for mouse btn press for widget to function.
    NOTE: this returns the Gtk.Gesture, you need to keep this around or it
    won't work. Assign it to some random variable and don't let it go out of
    scope'''

    gesture = Gtk.GestureMultiPress.new(widget)
    gesture.set_button(0)
    gesture.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
    gesture.connect('pressed', function)
    return gesture
