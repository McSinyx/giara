from gi.repository import Gtk, Handy, GLib
from giara.image_utils import set_avatar_func
from threading import Thread


class SimpleAvatar(Gtk.Bin):
    def __init__(self, size: int, title: str, get_image_func):
        super().__init__()
        self.avatar = Handy.Avatar.new(size, title, True)
        self.add(self.avatar)
        self.get_image_func = get_image_func
        self.load_avatar()

    def load_avatar(self):

        def af():
            icon = self.get_image_func()
            GLib.idle_add(cb, icon)

        def cb(icon):
            self.avatar.set_image_load_func(
                lambda size: set_avatar_func(icon, size)
            )

        Thread(target=af).start()
