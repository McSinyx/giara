from gi.repository import Gtk, Handy
from giara.left_stack import LeftStack
from giara.post_details_view import PostDetailsView
from giara.confManager import ConfManager
from giara.accel_manager import add_mouse_button_accel
from praw.models import Comment, Message


class MainDeck(Handy.Deck):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit

        self.left_stack = LeftStack(self.show_post)
        self.post_view_container = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL
        )
        self.add(self.left_stack)
        self.add(self.post_view_container)
        self.left_stack.set_size_request(300, 100)
        self.set_can_swipe_back(True)
        self.set_can_swipe_forward(False)

    def show_post(self, post):
        if isinstance(post, Message):
            return
        for child in self.post_view_container.get_children():
            self.post_view_container.remove(child)
        if isinstance(post, Comment):
            post = post.submission
        details_view = PostDetailsView(post, self.go_back)

        def on_mouse_event(gesture, n_press, x, y):
            if gesture.get_current_button() == 8:  # Mouse back btn
                self.go_back()

        details_view.mouse_btn_accel = add_mouse_button_accel(
            details_view, on_mouse_event
        )
        self.post_view_container.add(details_view)
        details_view.set_vexpand(True)
        details_view.set_hexpand(True)
        self.set_visible_child(self.post_view_container)
        details_view.show_all()

    def go_back(self):
        self.set_visible_child(self.left_stack)
