from gi.repository import Gtk, Pango
from giara.simple_avatar import SimpleAvatar


class CommonCollectionListboxRow(Gtk.ListBoxRow):
    def __init__(self, name, title, get_icon_func, avatar_name=None, **kwargs):
        super().__init__(**kwargs)
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.main_box.set_margin_start(12)
        self.main_box.set_margin_end(12)
        self.main_box.set_margin_top(12)
        self.main_box.set_margin_bottom(12)

        self.name = name
        self.title = title
        self.get_icon_func = get_icon_func

        self.avatar = SimpleAvatar(
            42,
            avatar_name or title,
            self.get_icon_func
        )

        self.label = Gtk.Label(
            title or name
        )
        self.desc_label = Gtk.Label(name)
        self.labels_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        for lbl in (self.label, self.desc_label):
            self.labels_box.add(lbl)
            lbl.set_line_wrap(True)
            lbl.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR)
            lbl.set_vexpand(True)
            lbl.set_hexpand(True)
            lbl.set_halign(Gtk.Align.START)
            lbl.set_justify(Gtk.Justification.FILL)
            lbl.set_margin_start(12)

        self.desc_label.get_style_context().add_class('subtitle')

        self.main_box.add(self.avatar)
        self.avatar.set_vexpand(False)
        self.avatar.set_hexpand(False)
        self.main_box.add(self.labels_box)
        self.add(self.main_box)

    def get_key(self):
        return self.name + ' ' + self.title
