from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from giara.path_utils import is_image
from giara.download_manager import download_img
from gi.repository import Gtk, Handy, GLib
from giara.sections_stack import SectionsStack
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from threading import Thread
from giara.time_utils import humanize_utc_timestamp
from giara.simple_avatar import SimpleAvatar
from giara.multireddit_list_view import MultiredditsListbox
from giara.confManager import ConfManager


class MultiPickerListbox(MultiredditsListbox):
    def __init__(self, sub, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sub = sub
        self.set_filter_func(self.filter_func, None, False)
        self.term = ''
        self.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.connect('row-activated', self.on_row_activate)
        self.working = False

    def add(self, row, *args, **kwargs):
        row.in_sub = False
        super().add(row, *args, **kwargs)

        def af():
            row.in_sub = self.sub in row.multi.subreddits
            GLib.idle_add(cb)

        def cb():
            if row.in_sub:
                icon = Gtk.Image.new_from_icon_name(
                    'emblem-ok-symbolic', Gtk.IconSize.BUTTON
                )
                icon.set_tooltip_text(_(
                    'This subreddit is already part of this multireddit'
                ))
                row.main_box.pack_end(icon, False, False, 6)
            row.show_all()

        Thread(target=af).start()

    def on_row_activate(self, lb, row):
        if self.working or row.in_sub:
            return
        self.working = True
        self.set_sensitive(False)

        def af():
            row.multi.add(self.sub)
            GLib.idle_add(cb)

        def cb():
            self.working = False
            self.populate()
            self.set_sensitive(True)

        Thread(target=af).start()

    def set_term(self, term):
        self.term = term.strip()
        self.invalidate_filter()

    def filter_func(self, row, data, notify_destroy):
        return not self.term or self.term.lower() in row.get_key().lower()


class SubredditViewMorePopover(Gtk.Popover):
    def __init__(self, sub, relative_to, **kwargs):
        super().__init__(**kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit
        self.sub = sub
        self.relative_to = relative_to
        self.set_modal(True)
        self.set_relative_to(self.relative_to)
        self.set_size_request(300, 400)
        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/subreddit_more_actions.glade'
        )
        self.stack = self.builder.get_object('stack')
        self.stack_container = self.builder.get_object('stack_container')
        self.add(self.stack_container)
        self.add_to_multi_box = self.builder.get_object('add_to_multi_box')
        self.add_to_multi_back_btn = self.builder.get_object(
            'add_to_multi_back_btn'
        )
        self.actions_buttons_box = self.builder.get_object(
            'actions_buttons_box'
        )
        self.add_to_multi_btn = self.builder.get_object('add_to_multi_btn')
        self.multi_picker_container = self.builder.get_object(
            'multi_picker_container'
        )
        self.add_to_multi_btn.connect(
            'clicked',
            lambda *args: self.stack.set_visible_child(self.add_to_multi_box)
        )
        self.add_to_multi_back_btn.connect(
            'clicked',
            lambda *args: self.stack.set_visible_child(
                self.actions_buttons_box
            )
        )

        self.picker_builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/choice_picker_popover_content.glade'
        )
        self.picker_main_container = self.picker_builder.get_object(
            'main_container'
        )
        self.multi_picker_container.add(self.picker_main_container)
        self.picker_main_container.set_vexpand(True)
        self.picker_main_container.set_hexpand(True)
        self.picker_main_container.set_margin_top(0)
        self.picker_main_container.set_margin_bottom(0)
        self.picker_main_container.set_margin_start(0)
        self.picker_main_container.set_margin_end(0)
        self.picker_sw = self.picker_builder.get_object('scrolled_win')
        self.picker_lbox = MultiPickerListbox(self.sub)
        self.picker_sw.add(self.picker_lbox)
        self.picker_search_entry = self.picker_builder.get_object(
            'search_entry'
        )
        self.picker_search_entry.connect(
            'changed',
            lambda *args: self.picker_lbox.set_term(
                self.picker_search_entry.get_text()
            )
        )

        self.new_multi_box = self.builder.get_object('new_multi_box')
        self.new_multi_name_entry = self.builder.get_object(
            'new_multi_name_entry'
        )
        self.new_multi_create_btn = self.builder.get_object(
            'new_multi_create_btn'
        )
        self.new_multi_btn = self.builder.get_object('new_multi_btn')
        self.new_multi_btn.connect(
            'clicked',
            lambda *args: self.stack.set_visible_child(
                self.new_multi_box
            )
        )
        self.new_multi_create_btn.connect('clicked', self.create_multi)
        self.new_multi_name_entry.connect(
            'changed', self.on_new_multi_name_entry_changed
        )
        self.new_multi_back_btn = self.builder.get_object(
            'new_multi_back_btn'
        )
        self.new_multi_back_btn.connect(
            'clicked', lambda *args: self.stack.set_visible_child(
                self.add_to_multi_box
            )
        )

    def get_new_multi_name(self, *args):
        return self.new_multi_name_entry.get_text().strip()

    def on_new_multi_name_entry_changed(self, *args):
        self.new_multi_create_btn.set_sensitive(
            not not self.get_new_multi_name()
        )

    def create_multi(self, *args):
        name = self.get_new_multi_name()
        if not name:
            return

        def af():
            self.reddit.multireddit.create(
                display_name=name,
                subreddits=[self.sub]
            )
            GLib.idle_add(cb)

        def cb():
            self.stack.set_visible_child(self.add_to_multi_box)
            self.new_multi_name_entry.set_text('')
            self.picker_lbox.populate()

        Thread(target=af).start()


class SubredditViewHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, stack, sub, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/subreddit_view_headerbar.glade'
            ),
            stack,
            **kwargs
        )
        self.sub = sub
        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.back_btn = self.builder.get_object('back_btn')
        self.search_btn = self.builder.get_object('search_btn')

        self.more_btn = Gtk.Button.new_from_icon_name(
            'view-more-symbolic', Gtk.IconSize.BUTTON
        )
        self.more_popover = SubredditViewMorePopover(self.sub, self.more_btn)
        self.more_btn.set_tooltip_text(_('More actions'))
        self.headerbar.pack_end(self.more_btn)
        self.more_btn.connect(
            'clicked',
            lambda *args: self.more_popover.popup()
        )


class SubredditHeading(Gtk.Bin):
    def __init__(self, sub, refresh_func, **kwargs):
        super().__init__(**kwargs)
        self.sub = sub
        self.refresh_func = refresh_func
        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/subreddit_heading.glade'
        )
        self.main_box = self.builder.get_object('main_box')
        self.avatar_container = self.builder.get_object('avatar_container')
        self.title_label = self.builder.get_object('title_label')
        self.title_label.set_text(self.sub.title)
        self.display_name_label = self.builder.get_object('display_name_label')
        self.display_name_label.set_text(self.sub.display_name_prefixed)
        self.join_btn = self.builder.get_object('join_btn')
        self.join_btn.connect('clicked', self.join_or_leave)
        self.refresh_join_btn()
        self.members_label = self.builder.get_object('members_label')
        self.members_label.set_text(str(self.sub.subscribers))
        self.since_label = self.builder.get_object('since_label')
        self.since_label.set_text(str(
            humanize_utc_timestamp(self.sub.created_utc)
        ))

        self.avatar = SimpleAvatar(
            64, self.sub.display_name, self.get_subreddit_icon
        )
        self.avatar_container.add(self.avatar)
        self.avatar.set_hexpand(False)
        self.avatar.set_vexpand(False)

        self.add(self.main_box)

    def refresh_join_btn(self, *args):
        join_style_context = self.join_btn.get_style_context()
        for c in ('suggested-action', 'destructive-action'):
            join_style_context.remove_class(c)
        if self.sub.user_is_subscriber:
            join_style_context.add_class('destructive-action')
            self.join_btn.set_label(_('Leave'))
        else:
            join_style_context.add_class('suggested-action')
            self.join_btn.set_label(_('Join'))

    def join_or_leave(self, *args):
        self.join_btn.set_sensitive(False)

        def af():
            if self.sub.user_is_subscriber:
                self.sub.unsubscribe()
            else:
                self.sub.subscribe()
            GLib.idle_add(cb)

        def cb():
            self.refresh_func()

        Thread(target=af).start()

    def get_subreddit_icon(self):
        if is_image(self.sub.icon_img):
            return download_img(self.sub.icon_img)


class SubredditView(Gtk.Box):
    def __init__(self, sub, show_post_func, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.sub = sub
        self.show_post_func = show_post_func
        self.sections_stack = SectionsStack([
            {
                'name': 'posts',
                'title': self.sub.display_name,
                'gen': self.sub.hot,
                'icon': 'hot-symbolic'
            },
            {
                'name': 'description',
                'title': _('Description'),
                'type': 'text',
                'text': self.sub.description,
                'icon': 'preferences-system-details-symbolic'
            }
            # TODO: add links section to this stack
        ], self.show_post_func)
        self.headerbar = SubredditViewHeaderbar(self.sections_stack, sub)
        self.headerbar.refresh_btn.connect('clicked', self.refresh)
        self.bottom_bar = Handy.ViewSwitcherBar()
        self.bottom_bar.set_stack(self.sections_stack)
        self.headerbar.connect(
            'squeeze',
            lambda caller, squeezed: self.bottom_bar.set_reveal(squeezed)
        )

        self.add_heading()

        self.add(self.headerbar)
        self.headerbar.set_vexpand(False)
        self.headerbar.set_hexpand(True)
        self.add(self.sections_stack)
        self.sections_stack.set_vexpand(True)
        self.add(self.bottom_bar)
        self.bottom_bar.set_vexpand(False)
        self.bottom_bar.set_hexpand(True)

    def add_heading(self):
        heading = SubredditHeading(self.sub, self.refresh)
        heading_lbox_row = Gtk.ListBoxRow()
        heading_lbox_row.add(heading)
        heading_lbox_row.get_style_context().add_class(
            'non-highlighted-listbox-row'
        )
        self.sections_stack.get_child_by_name(
            'posts'
        ).post_preview_lbox.insert(heading_lbox_row, 0)
        heading_lbox_row.show_all()

    def refresh(self, *args):

        def af():
            self.sub._fetch()
            GLib.idle_add(cb)

        def cb():
            self.sections_stack.refresh()
            self.add_heading()

        Thread(target=af).start()
