from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from gi.repository import Gtk, Handy, GLib
from giara.common_post_box import CommonPostBox, InteractiveEntityBox
from giara.new_post_window import NewCommentWindow
from giara.markdown_view import MarkdownView
from giara.confManager import ConfManager
from threading import Thread
from time import sleep
from giara.giara_clamp import GiaraClamp


class CommentBox(InteractiveEntityBox):
    def __init__(self, comment, refresh_func, me, level=0, **kwargs):
        super().__init__(
            comment,
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/comment_box.glade'
            ),
            **kwargs
        )
        self.me = me
        self.refresh_func = refresh_func
        self.comment = comment
        self.level = level
        self.reply_btn = self.builder.get_object('reply_btn')
        self.reply_btn.connect(
            'clicked',
            self.on_reply_clicked
        )
        self.author_label = self.builder.get_object('author_label')
        self.op_icon = self.builder.get_object('op_icon')
        self.me_icon = self.builder.get_object('me_icon')
        self.comment_label = self.builder.get_object('comment_label')
        self.replies_container = self.builder.get_object('replies_container')

        self.comment_container = self.builder.get_object('comment_container')
        self.body_view = MarkdownView(self.comment.body)
        self.comment_container.add(self.body_view)
        self.body_view.set_vexpand(False)
        self.body_view.set_hexpand(True)
        author_name = _('Author unknown')
        if hasattr(self.comment, 'author') and self.comment.author is not None:
            author_name = f'u/{self.comment.author.name}'
        self.author_label.set_text(author_name)

        if self.level > 0:
            self.builder.get_object(
                'main_box'
            ).get_style_context().add_class(f'nested-{(self.level - 1) % 8}')
        author_label_style_context = self.author_label.get_style_context()
        if self.comment.is_submitter:
            author_label_style_context.add_class('op_comment')
            self.op_icon.set_visible(True)
            self.op_icon.set_no_show_all(False)
        else:
            author_label_style_context.add_class('comment_author')
            self.op_icon.set_visible(False)
            self.op_icon.set_no_show_all(True)
        if self.comment.author == self.me:
            for sc in ('op_comment', 'comment_author'):
                author_label_style_context.remove_class(sc)
            author_label_style_context.add_class('green')
            self.me_icon.set_visible(True)
            self.me_icon.set_no_show_all(False)
        else:
            self.me_icon.set_visible(False)
            self.me_icon.set_no_show_all(True)

        GLib.idle_add(self.populate_replies)

    def populate_replies(self):
        for reply in self.comment.replies:
            n_reply = CommentBox(
                reply, self.refresh_func, self.me, self.level+1
            )
            self.replies_container.pack_start(
                n_reply,
                False,
                False,
                0
            )
            n_reply.show_all()

    def on_reply_clicked(self, *args):
        win = NewCommentWindow(
            self.comment,
            lambda *args: self.refresh_func(wait_for_comments_update=True)
        )
        win.set_transient_for(self.get_toplevel())
        win.present()
        win.show_all()


class MultiCommentsBox(Gtk.Box):
    def __init__(self, comments, refresh_func, me, level=0, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.me = me
        self.level = level
        self.refresh_func = refresh_func
        self.comments = comments
        self.populate()

    def populate(self, *args):

        def af():
            self.comments.replace_more(limit=None)
            GLib.idle_add(cb)

        def cb():
            for comment in self.comments:
                cbox = CommentBox(
                    comment, self.refresh_func, self.me, self.level
                )
                self.pack_start(
                    cbox,
                    False,
                    False,
                    6
                )
                cbox.show_all()

        Thread(target=af).start()


class PostBody(CommonPostBox):
    def __init__(self, post, refresh_func, **kwargs):
        super().__init__(
            post,
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/post_body.glade'
            ),
            **kwargs
        )
        self.body_view = MarkdownView(self.post.selftext)
        self.body_container = self.builder.get_object('body_container')
        self.body_container.add(self.body_view)
        self.body_view.set_vexpand(False)
        self.body_view.set_hexpand(True)
        self.refresh_func = refresh_func

        self.reply_btn = self.builder.get_object('reply_btn')
        self.reply_btn.connect(
            'clicked',
            self.on_reply_clicked
        )

    def on_reply_clicked(self, *args):
        win = NewCommentWindow(
            self.post,
            lambda *args: self.refresh_func(wait_for_comments_update=True)
        )
        win.set_transient_for(self.get_toplevel())
        win.present()
        win.show_all()


class PostDetailsHeaderbar(Handy.WindowHandle):
    def __init__(self, post, back_func, refresh_func, **kwargs):
        super().__init__(**kwargs)
        self.post = post
        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/post_details_headerbar.glade'
        )
        self.headerbar = self.builder.get_object('headerbar')
        self.headerbar.set_title(self.post.title)

        self.back_btn = self.builder.get_object('back_btn')
        self.back_btn.connect('clicked', lambda *args: back_func())

        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.refresh_btn.connect('clicked', lambda *args: refresh_func())

        self.add(self.headerbar)


class PostDetailsView(Gtk.ScrolledWindow):
    def __init__(self, post, back_func, **kwargs):
        super().__init__(**kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit
        self.post = post
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.inner_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.headerbar = PostDetailsHeaderbar(
            self.post, back_func, self.refresh
        )
        self.main_box.add(self.headerbar)
        self.headerbar.set_vexpand(False)
        self.headerbar.set_hexpand(True)

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        self.clamp = GiaraClamp()
        self.inner_box.get_style_context().add_class('card')
        self.clamp.add(self.inner_box)

        self.sw.add(self.clamp)
        self.main_box.add(self.sw)
        self.sw.set_vexpand(True)
        self.add(self.main_box)

        self.refresh(reload_post=False)

    def populate_inner_box(self):
        for child in self.inner_box.get_children():
            self.inner_box.remove(child)
        self.inner_box.add(self.post_body)
        self.post_body.set_vexpand(True)
        separator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.inner_box.add(separator)
        separator.set_vexpand(False)
        self.inner_box.add(self.multi_comments_box)
        self.inner_box.set_vexpand(False)
        self.show_all()

    def refresh(self, reload_post=True, wait_for_comments_update=False):
        self.headerbar.refresh_btn.set_sensitive(False)

        def af(callback):
            if reload_post:
                if wait_for_comments_update:
                    tries = 0
                    cl = self.post.comments.list()
                    self.post = self.reddit.submission(self.post.id)
                    while (
                            tries < 10 and
                            len(cl) == len(self.post.comments.list())
                    ):
                        tries += 1
                        sleep(1)
                        self.post = self.reddit.submission(self.post.id)
                else:
                    self.post = self.reddit.submission(self.post.id)
            GLib.idle_add(callback)

        def cb():
            self.post_body = PostBody(self.post, self.refresh)
            self.multi_comments_box = MultiCommentsBox(
                self.post.comments, self.refresh, self.reddit.user.me()
            )
            self.populate_inner_box()
            self.headerbar.refresh_btn.set_sensitive(True)

        Thread(target=af, args=(cb,)).start()
