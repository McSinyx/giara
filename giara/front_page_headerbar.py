from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from gi.repository import Gtk
from giara.confManager import ConfManager
from giara.new_post_window import NewPostWindow
from giara.download_manager import download_img
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.simple_avatar import SimpleAvatar


class FrontPageHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, front_page_stack, reddit, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/headerbar.glade'
            ),
            front_page_stack,
            **kwargs
        )
        self.reddit = reddit
        self.builder.connect_signals(self)
        self.confman = ConfManager()

        self.menu_btn = self.builder.get_object(
            'menu_btn'
        )
        self.menu_popover = Gtk.PopoverMenu()
        self.menu_builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/menu.xml'
        )
        self.menu = self.menu_builder.get_object('generalMenu')
        self.menu_popover.bind_model(self.menu)
        self.menu_popover.set_relative_to(self.menu_btn)
        self.menu_popover.set_modal(True)

        self.new_btn = self.builder.get_object('new_btn')
        self.new_post_popover = self.builder.get_object('new_post_popover')
        self.new_post_popover.set_modal(True)
        self.new_btn.connect(
            'clicked',
            lambda *args: self.new_post_popover.popup()
        )
        self.new_text_btn = self.builder.get_object('new_text_btn')
        self.new_text_btn.connect(
            'clicked',
            lambda *args: self.on_new_clicked('text')
        )
        self.new_link_btn = self.builder.get_object('new_link_btn')
        self.new_link_btn.connect(
            'clicked',
            lambda *args: self.on_new_clicked('link')
        )
        self.new_media_btn = self.builder.get_object('new_media_btn')
        self.new_media_btn.connect(
            'clicked',
            lambda *args: self.on_new_clicked('media')
        )

        self.profile_btn = self.builder.get_object('profile_btn')
        self.profile_popover = self.builder.get_object('profile_popover')
        self.username_label = self.builder.get_object('username_label')
        self.karma_label = self.builder.get_object('karma_label')
        self.user = self.reddit.user.me()
        self.username_label.set_text(f'u/{self.user.name}')
        self.karma_label.set_text(_('{0} Karma').format(self.user.total_karma))
        self.avatar_container = self.builder.get_object('avatar_container')
        self.avatar = SimpleAvatar(
            42, self.user.name, lambda *args: download_img(self.user.icon_img)
        )
        self.avatar_container.add(self.avatar)
        self.avatar_container.show_all()
        self.profile_btn.connect(
                'clicked',
                lambda *args: self.profile_popover.popup()
        )
        self.go_subreddits_btn = self.builder.get_object('go_subreddits_btn')
        self.go_subreddits_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )
        self.go_saved_btn = self.builder.get_object('go_saved_btn')
        self.go_saved_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_profile_btn = self.builder.get_object('go_profile_btn')
        self.go_profile_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_inbox_btn = self.builder.get_object('go_inbox_btn')
        self.go_inbox_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.go_logout_btn = self.builder.get_object('go_logout_btn')
        self.go_logout_btn.connect(
            'clicked',
            self.on_logout
        )

        self.go_multireddits_btn = self.builder.get_object(
            'go_multireddits_btn'
        )
        self.go_multireddits_btn.connect(
            'clicked',
            lambda *args: self.profile_popover.popdown()
        )

        self.inbox_badge = self.builder.get_object('inbox_count_badge_label')
        self.confman.connect(
            'notif_count_change',
            self.on_inbox_count_change
        )

        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.search_btn = self.builder.get_object('search_btn')

    def on_logout(self, *args):
        self.profile_popover.popdown()
        dialog = Gtk.MessageDialog(
            self.get_toplevel(),
            Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
            Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO,
            _('Do you want to log out? This will close the application.')
        )
        res = dialog.run()
        dialog.close()
        if res == Gtk.ResponseType.YES:
            self.confman.conf['refresh_token'] = ''
            self.confman.save_conf()
            self.get_toplevel().destroy()

    def on_inbox_count_change(self, caller, count):
        if int(count) <= 0:
            self.inbox_badge.set_text('')
            self.inbox_badge.set_visible(False)
            self.inbox_badge.set_no_show_all(True)
        else:
            self.inbox_badge.set_text(count)
            self.inbox_badge.set_visible(True)
            self.inbox_badge.set_no_show_all(False)
            self.inbox_badge.show()

    def on_menu_btn_clicked(self, *args):
        self.menu_popover.popup()

    def on_new_clicked(self, post_type):
        self.new_post_popover.popdown()
        np_win = NewPostWindow(self.reddit, post_type)
        np_win.set_transient_for(self.get_toplevel())
        np_win.present()
