from giara.constants import RESOURCE_PREFIX
from gi.repository import Gtk, GdkPixbuf, Gdk
from giara.confManager import ConfManager


class PictureView(Gtk.DrawingArea):
    def __init__(self, path, is_video=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.confman = ConfManager()
        self.is_video = is_video
        self.path = path

        self.video_icon_pixbuf = None
        if self.is_video:
            self.video_icon_pixbuf = (
                GdkPixbuf.Pixbuf.new_from_resource(
                    f'{RESOURCE_PREFIX}/icons/video-icon.svg'
                )
            )
        self.pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.path)
        self.connect('draw', self.on_draw)

    def get_useful_height(self, width):
        aw = width
        pw = self.pixbuf.get_width()
        ph = self.pixbuf.get_height()
        return aw/pw * ph

    def on_draw(self, area, context):
        width = area.get_allocated_width()
        x_pos = 0
        if self.confman.conf['max_picture_width'] > 0:
            if width > self.confman.conf['max_picture_width']:
                width = self.confman.conf['max_picture_width']
                x_pos = int(area.get_allocated_width()/2)-int(width/2)
        height = self.get_useful_height(width)
        self.set_size_request(-1, height)
        scaled_pixbuf = self.pixbuf.scale_simple(
            width, height, GdkPixbuf.InterpType.BILINEAR
        )
        Gdk.cairo_set_source_pixbuf(context, scaled_pixbuf, x_pos, 0)
        if self.is_video:
            context.paint()
            Gdk.cairo_set_source_pixbuf(
                context, self.video_icon_pixbuf,
                x_pos+int(width/2)-int(self.video_icon_pixbuf.get_width()/2),
                int(height/2)-int(self.video_icon_pixbuf.get_height()/2),
            )
        context.paint()
