from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from gi.repository import Gtk, Handy
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.sections_stack import SectionsStack
from giara.confManager import ConfManager


class SearchViewHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, stack, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/'
                'headerbar_with_back_and_squeezer.glade'
            ),
            stack,
            **kwargs
        )
        self.back_btn = self.builder.get_object('back_btn')


class CommonSearchView(Gtk.Box):
    def __init__(self, headerbar, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.confman = ConfManager()
        self.reddit = self.confman.reddit

        self.headerbar = headerbar
        self.searchbar = Handy.SearchBar()
        self.search_entry = Gtk.SearchEntry()
        self.search_entry.connect('activate', self.on_search_activate)
        self.searchbar.add(self.search_entry)
        self.searchbar.connect_entry(self.search_entry)

        self.add(self.headerbar)
        self.headerbar.set_vexpand(False)
        self.headerbar.set_hexpand(True)

        self.add(self.searchbar)
        self.searchbar.set_vexpand(False)
        self.searchbar.set_hexpand(True)
        self.searchbar.set_search_mode(True)

    def on_search_activate(self, *args):
        raise NotImplementedError()


class SearchView(CommonSearchView):
    def __init__(self, show_post_func, show_subreddit_func, **kwargs):
        self.show_post_func = show_post_func
        self.show_subreddit_func = show_subreddit_func
        self.sections_stack = SectionsStack(
            [
                {
                    'name': 'subreddits',
                    'title': _('Subreddits'),
                    'gen': None,
                    'type': 'subs',
                    'load_now': False,
                    'icon': 'org.gabmus.giara-symbolic'
                },
                {
                    'name': 'users',
                    'title': _('Users'),
                    'gen': None,
                    'type': 'subs',
                    'load_now': False,
                    'icon': 'avatar-default-symbolic'
                },
                # {
                #     'name': 'posts',
                #     'title': _('Posts'),
                #     'gen': None,
                #     'icon': 'text-editor-symbolic'
                # },
            ],
            self.show_post_func,
            show_subreddit_func=self.show_subreddit_func,
            load_now=False
        )
        super().__init__(
            SearchViewHeaderbar(self.sections_stack),
            **kwargs
        )

        self.bottom_bar = Handy.ViewSwitcherBar()
        self.bottom_bar.set_stack(self.sections_stack)
        self.headerbar.connect(
            'squeeze',
            lambda caller, squeezed: self.bottom_bar.set_reveal(squeezed)
        )
        self.add(self.sections_stack)
        self.sections_stack.set_vexpand(True)

        self.add(self.bottom_bar)
        self.bottom_bar.set_vexpand(False)

    def on_search_activate(self, *args):
        term = self.search_entry.get_text()
        subs_view = self.sections_stack.get_child_by_name(
            'subreddits'
        ).get_child().get_child().get_child()
        subs_view.set_gen_func(
            lambda *args, **kwargs: self.reddit.subreddits.search(
                term, *args, **kwargs
            )
        )
        subs_view.initialized = False
        users_view = self.sections_stack.get_child_by_name(
            'users'
        ).get_child().get_child().get_child()
        users_view.set_gen_func(
            lambda *args, **kwargs: self.reddit.redditors.search(
                term, *args, **kwargs
            )
        )
        users_view.initialized = False
        self.sections_stack.on_visible_child_change()
