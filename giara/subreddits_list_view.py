from gettext import gettext as _
from gi.repository import Gtk, GLib
from giara.path_utils import is_image
from giara.download_manager import download_img
from giara.single_post_stream_headerbar import SinglePostStreamHeaderbar
from threading import Thread
from praw.models import Redditor
from giara.giara_clamp import GiaraClamp
from giara.common_collection_listbox_row import CommonCollectionListboxRow


class SubredditsListboxRow(CommonCollectionListboxRow):
    def __init__(self, subreddit, **kwargs):
        self.subreddit = subreddit
        if isinstance(subreddit, Redditor):
            self.subreddit.display_name = self.subreddit.name
            self.subreddit.display_name_prefixed = 'u/'+self.subreddit.name
            self.subreddit.title = self.subreddit.name
        super().__init__(
            self.subreddit.display_name_prefixed,
            self.subreddit.title,
            self.get_subreddit_icon,
            avatar_name=self.subreddit.display_name,
            **kwargs
        )

    def get_subreddit_icon(self):
        if is_image(self.subreddit.icon_img):
            return download_img(self.subreddit.icon_img)


class SubredditsListbox(Gtk.ListBox):
    def __init__(self, subreddits_gen_func, show_sub_func=None, load_now=True,
                 sort=False, **kwargs):
        super().__init__(**kwargs)
        self.sort = sort
        self.get_style_context().add_class('card')
        self.subreddits_gen_func = subreddits_gen_func
        self.show_sub_func = show_sub_func
        self.subreddits = []
        self.connect('row-activated', self.on_row_activate)
        self.initialized = load_now
        self.refresh_thread = None
        if load_now:
            self.refresh()
        self.set_selection_mode(Gtk.SelectionMode.NONE)

        self.set_header_func(self.separator_header_func)

    def set_gen_func(self, gen_func):
        self.subreddits_gen_func = gen_func

    def separator_header_func(self, row, prev_row):
        if (
            prev_row is not None and
            row.get_header() is None
        ):
            row.set_header(
                Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
            )

    def empty(self):
        while True:
            row = self.get_row_at_index(0)
            if row:
                self.remove(row)
            else:
                break

    def refresh(self):
        self.empty()
        if self.refresh_thread is not None:
            self.refresh_thread_stop = True
            self.refresh_thread.join()
        self.refresh_thread_stop = False

        def af():
            self.subreddits = list(self.subreddits_gen_func(limit=None))
            if self.sort:
                self.subreddits.sort(key=lambda sub: sub.display_name.lower())
            for sub in self.subreddits:
                if self.refresh_thread_stop:
                    return
                sub.display_name  # fetch the object?
                GLib.idle_add(cb, sub)
            GLib.idle_add(final_cb)

        def cb(sub):
            self.add(SubredditsListboxRow(sub))

        def final_cb():
            self.initialized = True
            self.show_all()

        self.refresh_thread = Thread(target=af)
        self.refresh_thread.start()

    def add(self, *args, **kwargs):
        super().add(*args, **kwargs)
        self.show_all()

    def populate(self):
        for sub in self.subreddits:
            self.add(
                SubredditsListboxRow(sub)
            )
        self.show_all()
        self.initialized = True

    def on_row_activate(self, lb, row):
        if self.show_sub_func is not None:
            self.show_sub_func(row.subreddit)


class SubredditsListView(Gtk.Box):
    def __init__(self, subreddits_gen_func, show_sub_func,
                 sort=False, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self.subreddits_gen_func = subreddits_gen_func
        self.show_sub_func = show_sub_func

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.listbox = SubredditsListbox(
            self.subreddits_gen_func, self.show_sub_func, sort=sort
        )

        self.clamp = GiaraClamp()
        self.clamp.add(self.listbox)

        self.sw.add(self.clamp)

        self.headerbar = SinglePostStreamHeaderbar(_('Subreddits'))
        self.headerbar.refresh_btn.connect(
            'clicked',
            lambda *args: self.listbox.refresh()
        )

        self.add(self.headerbar)
        self.headerbar.set_vexpand(False)
        self.headerbar.set_hexpand(True)
        self.add(self.sw)
        self.sw.set_vexpand(True)
