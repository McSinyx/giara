from giara.constants import RESOURCE_PREFIX
from gi.repository import Gtk, Handy


class SinglePostStreamHeaderbar(Handy.WindowHandle):
    def __init__(self, title, **kwargs):
        super().__init__(**kwargs)
        self.title = title

        self.builder = Gtk.Builder.new_from_resource(
            f'{RESOURCE_PREFIX}/ui/post_details_headerbar.glade'
        )
        self.headerbar = self.builder.get_object('headerbar')
        self.headerbar.set_title(self.title)
        self.back_btn = self.builder.get_object('back_btn')
        self.refresh_btn = self.builder.get_object('refresh_btn')
        self.add(self.headerbar)
